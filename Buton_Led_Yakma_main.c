/* 
 Burada kart üzerinde bulunan butona basarak iki tane değer girilmesini istiyoruz.
 Bu değerler arasında biraz süre bırakarak ikinci değerin girilmesini sağlıyoruz. 
 En sonunda ise bu değerleri çapım sonucu kadar led yakılmasını sağlıyoruz.
*/
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
void ledyak(int a, int b); //BURADA FONKSİYONLARIMIZI TANIMLIYORUZ.
void sifirlama();

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s3;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim13;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM13_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//BURADA GEREKLİ DE�?İ�?KENLERİMİZİ TANIMLIYORUZ.

int butonbasili,butonokuma,deger1,deger2,milisaniye,sayac,surebaslatma,saniye;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2S3_Init();
  MX_SPI1_Init();
  MX_USB_HOST_Init();
  MX_TIM13_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim13);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */

    butonokuma= HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0); //BURADA BUTONA BASILIP BASILMADI�?INI OKUYORUZ (1=BASILI,0=BASILMADI)

    if (butonokuma==1 && butonbasili==0) { //E�?ER BUTONA BASILI VE DAHA ÖNCE BASILMADIYSA GİR
    	butonbasili=1;						//SÜREKLİ OLARAK if İÇERİSİNE GİRMEMESİNİ SA�?LAMAK AMACIYLA 1 HALİNE GETİRİYORUZ
    	if (sayac==0) deger1++;				//E�?ER İLK KEZ BASILIYORSA değer1 İ ARTTIR.
    	else deger2++;						// E�?ER BASTIKTAN SONRA 2 SANİYE GEÇTİYSE A�?A�?IDA SAYACI ARTTIRARARK İKİNCİ DE�?ERİ OKUMASINI SA�?LIYORUZ.
    	HAL_Delay(200);						// ANLIK OLARAK GÜRÜLTÜDEN ETKİLENMEMESİNİ SA�?LAMAK AMACIYLA 250 MS BEKLETİYORUZ.
    }
    else if (butonokuma==0 && butonbasili==1){	//BUTONA BASIP ÇEKTİ�?İ ZAMAN DE�?ER SIFIR GELİYORSA butonbasili YI 0 YAP VE GEÇER SÜREYİ A�?A�?IDA BA�?LATMAK İÇİN SÜREYİ BA�?LAT VE SÜREYİ SIFIRLA
    	butonbasili=0;
    	surebaslatma=1;
    	milisaniye=0;
    }
    saniye=milisaniye/1000; //SANİYE Yİ BULUYORUZ


    if (butonbasili==0 && saniye>1){ //E�?ER SÜRE 2 SANİYEYİ GEÇİYORSA BİR SONRAKİ DE�?ER OKUMAYA GEÇİYORUZ
    	surebaslatma=0;	//SÜREYİ SIFIRLAYIP BA�?LATMAYI SIFIRLIYORUZ.
    	sayac++;	//YUKARIDA BİR SONRAKİ DE�?ERE KAYDETMESİ İÇİN YUKARIDA SAYACI ARTTIRARAK İKİNCİ DE�?ERİ OKUMASINI SA�?LIYORUZ.
    	milisaniye=0;
    }
    	if (sayac>1 && deger2!=0 && saniye>1){	//İKİ DE�?ERİ OKUDUYSA İF İÇERİSİNE GERMESİNİ SA�?LIYORUZ.
        	HAL_Delay(200); //Ledi görebilmek için bekletiyoruz.
    		ledyak(deger1,deger2);	//OKUNAN İKİ DE�?ERİ LEDLERİ YAKMASI İÇİN FONKSİYONA GÖNDERİYORUZ.
    	}

  }
  /* USER CODE END 3 */
}


/* USER CODE BEGIN 4 */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim==&htim13)
	{

		if (surebaslatma==1) milisaniye++;

	}
}

void ledyak(int a, int b){ // LEDLERİ YAKIP KAPATIYORUZ

	for (int c=0;c<a*b;c++){
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 1);
	HAL_Delay(400);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, 0);
	HAL_Delay(400);
	}
	HAL_Delay(100);
	sifirlama();	//EN SONUNDA TEKRAR BA�?LAMASINI SA�?LIYORUZ.
}

void sifirlama(){ //BÜTÜN DE�?ERLERİ SIFIRLATARAK TEKRAR BA�?LAMASINI SA�?LIYORUZ.
	butonbasili=0;
	butonokuma=0;
	deger1=0;
	deger2=0;
	milisaniye=0;
	sayac=0;
	surebaslatma=0;
	saniye=0;
}

/* USER CODE END 4 */
